var sys = require('sys');
var exec = require('child_process').exec;
var path = require('path');
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var pathToRepos = '../';
var secret = "203234-2342-3244-sfsd-s2";
var branchName = "deploy";

app.use(bodyParser());

app.post('/' + secret, function (req, res) {
    var data = JSON.parse(req.body.payload);
    exec("git pull origin " + branchName, {cwd: path.resolve(pathToRepos) + '/' + data.repository.name});
    res.status(200).end();
});

app.listen(9999);
